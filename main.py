#!/usr/bin/env python
#                          __      __              __    __            __           __
#    ________  ____ ____  / /     / /_  __  ______/ /___/ /_  __      / /_  ____   / /
#   / ___/ _ \/ __ `/ _ \/ /_____/ __ \/ / / / __  / __  / / / /_____/ __ \/ __ \ / /_
#  (__  )  __/ /_/ /  __/ /_____/ /_/ / /_/ / /_/ / /_/ / /_/ /_____/ /_/ / /_/ // __/
# /____/\___/\__, /\___/_/     /_.___/\__,_/\__,_/\__,_/\__, /     /_.___/\____// /_   
#           /____/                                     /____/                   \__/ 
#
# Programmer: Asaf Pincu
# Date: 01/07/2018
#

# Imports
import logging
import os
from dotenv import load_dotenv
from datetime import datetime, time
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                         ConversationHandler, RegexHandler)
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

# Load dotenv variables
load_dotenv('./.env')

# Constants
BOT_TOKEN = os.getenv('BOT_TOKEN')
EVENT_TIME, EVENT_DESCRIPTION, EVENT_CONFIRM = range(3)
DATE_TIME_REGEX                              = r'^(\d{1,2}/\d{1,2}/\d{4})(?:\s(\d{1,2}:\d{1,2}|))?$'
DATE_TIME_FORMAT                             = '%d/%m/%Y %H:%M'
DATE_INDEX                                   = 0
TIME_INDEX                                   = 1
MON, TUE, WED, THU, FRI, SAT, SUN            = range(7)
CONFIRM_KEYBOARD                             = [['Yes'],
                                                ['No']]
HELP_KEYBOARD                                = [['/setgzarot'],
                                                ['/setevent']]
DEFAULT_EVENT_TIME                           = '08:00'

def commandStart(bot, update):
    """
    commandStart(telegram.bot, updater.bot) --> None

    This callback sends a menu of possible commands to the user which requested it.
    """
    update.message.reply_text('Hey guys! let\'s start with what you can do-',reply_markup = ReplyKeyboardMarkup(HELP_KEYBOARD,
                                                                    one_time_keyboard= True,
                                                                    selective= True))

def commandSetEvent(bot, update):
    """
    commandSetEvent(telegram.bot, updater) --> None

    This callback function starts the conversation for setting an event.
    """
    update.message.reply_text('Great, Let\'s get started!\nWhen will the event happen?\nFormat as DD/MM/YYYY HH:MM.\n'\
                              'Only specify DD/MM/YYYY to get alerted at 08:00 AM.')
    return EVENT_TIME

def commandGetEventDateTime(bot, update, user_data, groups):
    """
    commandGetEventDateTime(telegram.bot, updater) --> None

    This callback function parses the time given to us and then passes it forward.
    """

    # If only the date was passed along
    if not groups[TIME_INDEX]:
        requestedTime = '{} {}'.format(update.message.text, DEFAULT_EVENT_TIME)
    else:
        # Get the time from the message.
        requestedTime = update.message.text

    # Parse the datetime string into a datetime object
    parsedDateTime = datetime.strptime(requestedTime, DATE_TIME_FORMAT)

    # Store the datetime into a the user_data dictionary
    user_data['datetime'] = parsedDateTime

    # Continue the conversation
    update.message.reply_text('Ok, what is the description of the event?')
    return EVENT_DESCRIPTION

def commandGetEventDescription(bot, update, user_data):
    """
    commandGetEventDescription(telegram.bot, updater) --> None
    """

    # Store the description in the user_data dictionary
    user_data['description'] = update.message.text

    # Continue the conversation
    update.message.reply_text('Ok so an update is set to {}\n"{}"\n\n Is it okay?'.format(
                              str(user_data['datetime']),
                              user_data['description']),
                              reply_markup=ReplyKeyboardMarkup(CONFIRM_KEYBOARD,
                                                               one_time_keyboard=True,
                                                               selective= True))

    return EVENT_CONFIRM

def commandConfirmEvent(bot, update, user_data, job_queue):
    """
    commandConfirmEvent(telegram.bot, updater) --> None
    """

    # Set the needed variables
    eventDateTime = user_data['datetime']
    eventDescription = user_data['description']

    # If the event is accepted
    if update.message.text == 'Yes':
        update.message.reply_text('Got it. setting an alarm.', reply_markup=ReplyKeyboardRemove())
        job_queue.run_once(lambda call_bot, call_job: eventCallback(call_bot, call_job, eventDescription),
                           eventDateTime,
                           update.message.chat_id)
    else:
        update.message.reply_text('Ok, nevermind then.', reply_markup = ReplyKeyboardRemove())

    # Clean the user_data dictionary from the values
    if 'datetime' in user_data:
        del user_data['datetime']

    if 'description' in user_data:
        del user_data['description']

def commandSetGzarot(bot, update, job_queue):
    """
    commandSetGzarot(telegram.bot, update) --> None
    This function is responsible for setting a gzarot alarm per course.
    """

    # Set the datetime.time objects for all morning, noon and evening.
    morning = time(hour=8, minute=45)
    noon = time(hour=13, minute=15)
    evening = time(hour=19, minute=30)

    # Set the jobs
    job_queue.run_daily(gzarotCallback, morning, (SUN, MON, TUE, WED, THU, FRI),
                        update.message.chat_id, 'gzarot_morning')
    job_queue.run_daily(gzarotCallback, noon, (SUN, MON, TUE, WED, THU, FRI),
                        update.message.chat_id, 'gzarot_noon')
    job_queue.run_daily(gzarotCallback, evening, (SUN, MON, TUE, WED, THU, FRI),
                        update.message.chat_id, 'gzarot_evening')

def commandCancelEvent(bot, update, user_data):
    """
    commandCancelEvent(telegram.bot, updater) --> None
    """

    # Send a cancel message.
    update.message.reply_text('No problemo. canceling the event.', reply_markup = ReplyKeyboardRemove())

    # Clean the user_data dictionary from the stored values
    if 'datetime' in user_data:
        del user_data['datetime']

    if 'description' in user_data:
        del user_data['description']

    return ConversationHandler.END

def gzarotCallback(bot, job):
    """
    gzarotCallback(telegram.bot, job) --> None
    This Callback function is responsible for sending out an gzarot alarm.
    """
    bot.send_message(job.context, text='!-G-Z-A-R-O-T-!')

def eventCallback(bot, job, alert_message):
    """
    eventCallback(telegram.bot, job, string) --> None
    This callback function is responsible for sending out the job queues message.
    """
    bot.send_message(job.context,alert_message)

def main():

    # Set the bot updater and dispatcher
    botUpdater = Updater(BOT_TOKEN)
    botDispatcher = botUpdater.dispatcher

    # Add the functions to the dispatcher
    botDispatcher.add_handler(CommandHandler('start', commandStart))
    botDispatcher.add_handler(CommandHandler('setgzarot', commandSetGzarot, pass_job_queue=True))
    botDispatcher.add_handler(ConversationHandler(
                                entry_points=[CommandHandler('setevent', commandSetEvent)],

                                states={
                                    EVENT_TIME: [RegexHandler(DATE_TIME_REGEX,
                                                              commandGetEventDateTime,
                                                              pass_groups=True,
                                                              pass_user_data=True)],

                                    EVENT_DESCRIPTION: [MessageHandler(Filters.text,
                                                                       commandGetEventDescription,
                                                                       pass_user_data=True)],

                                    EVENT_CONFIRM: [MessageHandler(Filters.text,
                                                                   commandConfirmEvent,
                                                                   pass_user_data=True,
                                                                   pass_job_queue=True)]},

                                fallbacks=[CommandHandler('cancelevent', commandCancelEvent, pass_user_data=True)]
                                ))

    # Start the bot
    botUpdater.start_polling()

    # Set the updater into idle (until it recives SIGTERM or SIGKILL)
    botUpdater.idle()

if __name__ == '__main__':
    main()
